$(document).ready(()=>{
	let catalog = $('.catalog').children(),
		buttons = $('.buttons').children();

	(function(){
		let about = $('.bounceIn');
		$(about).each((i)=>{
			setTimeout(()=>{
				$(about[i]).addClass('bounceInLeft');
			},i*600);
		});
	})();

	function catalogBounce() {
		let aScroll = $(window).scrollTop();
		
		if($('.catalog').offset().top - $(window).height()/2 < aScroll) {
			catalog.each((i)=>{
				setTimeout(()=>{
					catalog.eq(i).addClass('bounceInRight')
				},i*200)
			})
		};
	};

	function socialBounce() {
		let aScroll = $(window).scrollTop();
		
		if($('.buttons').offset().top - $(window).height()/2 < aScroll) {
			buttons.each((i)=>{
				setTimeout(()=>{
					buttons.eq(i).addClass('bounceInLeft')
				},i*200)
			})
		}
	};

	$(window).scroll(()=>{
		catalogBounce();
		socialBounce();	
	})
});